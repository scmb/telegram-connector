# Telegram Connector #
## Netgrid's [Bauer](https://bitbucket.org/netgrid/bauer) connector for Telegram's API##

The telegram-connector is a configurable java stand-alone application that allows to bauer-based applications to have direct access to Telegram's API. 
It works simply by providing a properties file as configuration where you specify the Telegram bot token used by your application and which bauer topics the connector have to interface.

## Configuration Example ##

```
#!java
bot_token=12345678:abcDeFGhijkLMnOPQRStuvWZYz
send_topic=telegram_tx 
receive_topic=telegram_rx
error_topic=telegram_error

```

## Interfacing your app ##
### Message Objects ###
The bauer topics in your application that you want to interface with the connector need to be initialized with a class like this:
```
#!java
public class TelegramConnectorMessage {
	private String sender;
	private String content;
	private Long timestamp;
	private Long chat_id;
	public TelegramConnectorMessage() {

	}
        //getters and setters for sender, content, timestamp, chat_Id
}
```
Except for the error topic object that needs to have a structure like this:
```
#!java
public class TelegramConnectorError {
	private String sender;
	private String content;

	public TelegramConnectorError() {

	}
        //getters and setters for sender and content
}
```
### Setting Up Topics ###
```
#!java
Topic<TelegramConnectorMessage> publishToTelegramTopic=TopicFactory.getTopic("telegram_rx");
Topic<TelegramConnectorMessage> receiveFromTelegramTopic=TopicFactory.getTopic("telegram_tx");
Topic<TelegramConnectorError> telegramErrorTopic=TopicFactory.getTopic("telegram_err");

receiveFromTelegramTopic.addHandler(new EventHandler<TelegramConnectorMessage>() {
			@Override
			public Class<TelegramConnectorMessage> getEventClass() {
				return TelegramConnectorMessage.class;
			}
			@Override
			public String getName() {
				return receiveFromTelegramTopic.getName();
			}
			@Override
			public boolean handle(TelegramConnectorMessage event) {
				System.out.println("Received message from chat "+event.getChat_id()+": "+event.getContent());
				// your code
				return true;
			}
		});

telegramErrorTopic.addHandler(new EventHandler<TelegramConnectorError>() {
			@Override
			public Class<TelegramConnectorError> getEventClass() {
				return TelegramConnectorError.class;
			}
			@Override
			public String getName() {
				return telegramErrorTopic.getName();
			}
			@Override
			public boolean handle(TelegramConnectorError event) {
				System.out.println("Telegram connector error: "+event.getContent());
				// your code
				return true;
			}
		});
```
### Posting Messages for the connector ###
```
#!java
TelegramConnectorMessage message=new TelegramConnectorMessage();
message.setSender("application name or application module");
long chatId=1235333234234 //number identifier of the chat
message.setChat_id(chatId);
message.setTimestamp(new Date().getTime());
message.setContent("the message");

publishToTelegramTopic.post(message);
```

## Run command ##
```
#!bash
java -jar telegram-connector.jar
```

## How do I get set up? ##

* Include the connector's jar in your runtime directory;
* Create the "telegram.properties" configuration file base on example and be sure the "bauer.properties" file is there too;
* Execute the application;
* Enjoy the fun my dude.

Version: 0.0.3