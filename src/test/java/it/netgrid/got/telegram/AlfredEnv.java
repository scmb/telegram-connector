package it.netgrid.got.telegram;

import com.google.guiceberry.GuiceBerryModule;
import com.google.inject.AbstractModule;

import it.netgrid.got.telegram.inject.TelegramConnectorModule;

public class AlfredEnv extends AbstractModule {

	@Override
	protected void configure() {
		install(new TelegramConnectorModule());
		install(new GuiceBerryModule());
	}

}
