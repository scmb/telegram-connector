package it.netgrid.got.telegram;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Guice;
import com.google.inject.Injector;

import it.netgrid.got.model.BauerModule;
import it.netgrid.got.telegram.inject.TelegramConnectorModule;

public class Main {
	static final int POOL_SIZE=1;
	public static void main(String[] args) {
		ExecutorService executor=Executors.newFixedThreadPool(POOL_SIZE);
		Injector injector = Guice.createInjector(new TelegramConnectorModule(), new BauerModule(args[0]));
		TelegramConnector connector = (injector.getInstance(TelegramConnector.class));
		executor.execute(connector);
	}
}
