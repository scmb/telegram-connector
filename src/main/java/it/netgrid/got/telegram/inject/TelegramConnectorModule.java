package it.netgrid.got.telegram.inject;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.bauer.EventHandler;
import it.netgrid.bauer.Topic;
import it.netgrid.got.model.MessageFromTelegram;
import it.netgrid.got.model.MessageToTelegram;
import it.netgrid.got.model.TelegramError;
import it.netgrid.got.telegram.MessageConverter;
import it.netgrid.got.telegram.MessageSender;
import it.netgrid.got.telegram.MessageToTelegramEventHandler;
import it.netgrid.got.telegram.TelegramConnector;
import it.netgrid.got.telegram.properties.TelegramPropertiesConfiguration;
import it.netgrid.got.telegram.properties.TelegramPropertiesConfigurationImplementation;
import me.shib.java.lib.jtelebot.service.TelegramBot;

public class TelegramConnectorModule extends AbstractModule {

	@Override
	protected void configure() {
	}

	@Provides
	public TelegramConnector getConnector(TelegramBot bot, Topic<MessageFromTelegram> fromTelegramTopic, Topic<TelegramError> errorTopic,
			Topic<MessageToTelegram> toTelegramTopic, MessageConverter converter,  EventHandler<MessageToTelegram> toTelegramHandler){
		return new TelegramConnector(bot,fromTelegramTopic,errorTopic,toTelegramTopic, converter,toTelegramHandler);	
	}
	
	@Provides
	@Singleton
	public MessageSender buildSender(TelegramBot bot,Topic<TelegramError> errorTopic){
		return new MessageSender( bot,errorTopic);
	}
	
	@Singleton
	@Provides
	public MessageConverter buildConverter(TelegramPropertiesConfiguration config){
		return new MessageConverter(config);
	}
	
	@Singleton
	@Provides
	public TelegramBot buildTelegramBot(TelegramPropertiesConfiguration config) {
		return TelegramBot.getInstance(config.getBotToken());
	}

	@Singleton
	@Provides
	public TelegramPropertiesConfiguration buildConfiguration() {
		return new TelegramPropertiesConfigurationImplementation();
	}
	
	@Provides
	public EventHandler<MessageToTelegram> buildMessageToTelegramHandler(MessageSender sender){
		return new MessageToTelegramEventHandler(sender);
	}
}