package it.netgrid.got.telegram;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.netgrid.bauer.EventHandler;
import it.netgrid.got.model.MessageToTelegram;

public class MessageToTelegramEventHandler implements EventHandler<MessageToTelegram> {

	private static final String HANDLER_NAME="telegram_connector_receive_handler";
	private static final Logger log=LoggerFactory.getLogger(MessageToTelegramEventHandler.class);
	MessageSender sender;
	
	public MessageToTelegramEventHandler(MessageSender sender){
		this.sender=sender;
	}
	
	@Override
	public Class<MessageToTelegram> getEventClass() {
		return MessageToTelegram.class;
	}

	@Override
	public String getName() {
		return HANDLER_NAME;
	}

	@Override
	public boolean handle(MessageToTelegram event) throws Exception {
		log.debug("Received message for chat %d",event.getChat_id());
		sender.send(event.getChat_id(), event.getContent());
		return true;
	}
}