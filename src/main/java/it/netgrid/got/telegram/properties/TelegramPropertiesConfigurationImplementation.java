package it.netgrid.got.telegram.properties;

import java.util.Properties;

import it.netgrid.got.utils.PropertiesConfigurationTemplate;


public class TelegramPropertiesConfigurationImplementation extends PropertiesConfigurationTemplate
		implements TelegramPropertiesConfiguration {

	private static final String BOT_TOKEN = "bot_token";

	private static final String DEFAULT_CONFIG_PROPERTIES_PATH =System.getProperty("user.dir")+"/telegram.properties";
	private static final String DEFAULT_CONFIG_PROPERTIES_RESOURCE = "telegram.properties";

	private Properties properties;

	public TelegramPropertiesConfigurationImplementation() {
		properties = getProperties(null);
	}

	@Override
	public String getBotToken() {
		return properties.getProperty(BOT_TOKEN);
	}

	@Override
	public String getDefaultConfigPropertiesPath() {
		return DEFAULT_CONFIG_PROPERTIES_PATH;
	}

	@Override
	public String getDefaultConfigPropertiesResource() {
		return DEFAULT_CONFIG_PROPERTIES_RESOURCE;
	}

}
