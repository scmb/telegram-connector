package it.netgrid.got.telegram;




import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import com.google.gson.Gson;

import it.netgrid.got.model.MessageFromTelegram;
import it.netgrid.got.telegram.gson.FileResponse;
import it.netgrid.got.telegram.properties.TelegramPropertiesConfiguration;
import me.shib.java.lib.jtelebot.models.types.PhotoSize;
import me.shib.java.lib.jtelebot.models.updates.Message;

public class MessageConverter {
	private TelegramPropertiesConfiguration conf;
	Gson gson;
	public MessageConverter(TelegramPropertiesConfiguration conf){
		this.conf=conf;
		gson=new Gson();
	}
	
	private String ADMIN_USER="sadkebab";
	private String PATH_REQUEST="https://api.telegram.org/bot%s/getFile?file_id=%s";
	private String DOWNLOAD_URL="https://api.telegram.org/file/bot%s/%s";

	public MessageFromTelegram convert(Message message){
		MessageFromTelegram ret=new MessageFromTelegram();
		ret.setSender(TelegramPropertiesConfiguration.MODULE_NAME);
		ret.setTimestamp(message.getDate());
		ret.setChat_id(message.getChat().getId());
		ret.setUserId(message.getChat().getUsername());
		ret.setIsPhoto(message.getPhoto()!=null);
		if(ret.isPhoto()&&message.getChat().getUsername().equals(ADMIN_USER)){
			ret.setPhotoURL(this.getUrl(message.getPhoto()));
		}
		ret.setIsAudio(message.getAudio()!=null);
		ret.setIsSticker(message.getSticker()!=null);
		ret.setIsVideo(message.getVideo()!=null);
		ret.setIsVoice(message.getVoice()!=null);
		ret.setContent(message.getText());
		System.out.println(ret.getPhotoURL());
		return ret;
	}

	private String getUrl(PhotoSize[] photo) {
		try{
		PhotoSize ph;
		if(photo.length==1){
			ph=photo[0];
		}else{
			ph=photo[photo.length-2];
		}
		InputStream stream=new URL(String.format(PATH_REQUEST,conf.getBotToken(),ph.getFile_id())).openStream();
		String path=gson.fromJson(new InputStreamReader(stream), FileResponse.class).getResult().getFilePath();
		return String.format(DOWNLOAD_URL, conf.getBotToken(), path);
		}
		catch(Exception e){
			return "";
		}
	}
	
}
