package it.netgrid.got.telegram.gson;

public class FileResponse {
	private boolean ok;
	private FileResult result;
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	public FileResult getResult() {
		return result;
	}
	public void setResult(FileResult result) {
		this.result = result;
	}
	
	
}
