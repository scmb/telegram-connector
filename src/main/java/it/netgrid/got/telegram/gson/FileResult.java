package it.netgrid.got.telegram.gson;

import com.google.gson.annotations.SerializedName;

public class FileResult {
	@SerializedName("file_id")
	private String fileId;
	@SerializedName("file_size")
	private String fileSize;
	@SerializedName("file_path")
	private String filePath;
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
}
