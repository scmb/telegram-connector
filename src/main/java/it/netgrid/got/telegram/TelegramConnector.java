package it.netgrid.got.telegram;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.netgrid.bauer.EventHandler;
import it.netgrid.bauer.Topic;
import it.netgrid.got.model.MessageFromTelegram;
import it.netgrid.got.model.MessageToTelegram;
import it.netgrid.got.model.TelegramError;
import it.netgrid.got.telegram.properties.TelegramPropertiesConfiguration;

import com.google.inject.Inject;

import me.shib.java.lib.jtelebot.models.updates.Message;
import me.shib.java.lib.jtelebot.models.updates.Update;
import me.shib.java.lib.jtelebot.service.TelegramBot;

public class TelegramConnector implements Runnable {
	
	private static final Logger log= LoggerFactory.getLogger(TelegramConnector.class);
	
	private final TelegramBot bot;
	private final Topic<MessageFromTelegram> fromTelegramTopic;
	private final Topic<TelegramError> errorTopic;
	private EventHandler<MessageToTelegram> toTelegramHandler;
	private Topic<MessageToTelegram> toTelegramTopic;
	private MessageConverter converter;
	
	@Inject
	public TelegramConnector(TelegramBot bot, Topic<MessageFromTelegram> fromTelegramTopic, Topic<TelegramError> errorTopic,
			Topic<MessageToTelegram> toTelegramTopic, MessageConverter converter,  EventHandler<MessageToTelegram> toTelegramHandler) {
		this.bot = bot;
		this.fromTelegramTopic = fromTelegramTopic;
		this.errorTopic = errorTopic;
		this.toTelegramTopic = toTelegramTopic;
		this.converter=converter;
		this.toTelegramHandler=toTelegramHandler;
	}

	@Override
	public void run() {
		toTelegramTopic.addHandler(toTelegramHandler);
		Update[] updates;
		try {
			while ((updates = bot.getUpdates()) != null) {
				for (Update update : updates) {
						Message message = update.getMessage();
						MessageFromTelegram event = converter.convert(message);
						fromTelegramTopic.post(event);
					} 
				}
		} catch (IOException e) {
			log.error("Failed to read bot updates... "+e.getMessage());
			errorTopic.post(new TelegramError(TelegramPropertiesConfiguration.MODULE_NAME, e.getMessage()));
		}

	}

}
