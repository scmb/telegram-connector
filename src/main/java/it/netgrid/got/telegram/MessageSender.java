package it.netgrid.got.telegram;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import it.netgrid.bauer.Topic;
import it.netgrid.got.model.TelegramError;
import it.netgrid.got.telegram.properties.TelegramPropertiesConfiguration;
import me.shib.java.lib.jtelebot.models.types.ChatId;
import me.shib.java.lib.jtelebot.service.TelegramBot;

@Singleton
public class MessageSender {
	private final TelegramBot bot;
	private static final Logger log =LoggerFactory.getLogger(MessageSender.class);
	private Topic<TelegramError> errorTopic;
	@Inject
	public MessageSender(TelegramBot bot,Topic<TelegramError> errorTopic){
		this.bot=bot;
		this.errorTopic=errorTopic;
	}

	public void send(Long chatId, String response){
		try {
			bot.sendMessage(new ChatId(chatId), response);
		} catch (IOException e) {
			log.error("Failed to send message... "+e.getMessage());
			errorTopic.post(new TelegramError(TelegramPropertiesConfiguration.MODULE_NAME, e.getMessage()));
		}
	}

}
